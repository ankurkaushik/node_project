var router = require('express').Router();
var auth = require('../config/tokencheck');
var authCtrl = new auth();
var multer  = require('multer');
// var upload = multer({ dest: 'public/uploads/' },preservePath)
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })
  var upload = multer({ storage: storage })
var userController = require('../controller/userController');
var userAPI = new userController();
var routeMothed = [userAPI.userChecking,userAPI.encryptPassword, userAPI.userSaving];
var loginRouteMethod = [userAPI.loginuserCheck,userAPI.passwordCompare, userAPI.assignJWT]


router.post('/',routeMothed);
router.post('/login',loginRouteMethod);
router.post('/save_product',authCtrl.tokencheck,upload.single('productImage'),userAPI.saveProduct);
router.get('/get_product_list',userAPI.getProductList);
router.post('/update_user_profile',authCtrl.tokencheck,userAPI.updateUser);
router.get('/paging/:pageNo',authCtrl.tokencheck,userAPI.paging);
router.get('/global_search/:searchValue', authCtrl.tokencheck,userAPI.GlobalSearch);
router.post('/add_to_cart/:id',authCtrl.tokencheck,userAPI.addToCart);
router.get('/get_cart_item/:id',authCtrl.tokencheck,userAPI.getCartItem);
router.post('/remove_to_cart/:id',authCtrl.tokencheck,userAPI.removeCart);
router.get('/get_particular_product/:id',userAPI.getOneProduct);
router.get('/buy_now/:uid/:pid',authCtrl.tokencheck,userAPI.startBuyNow);
router.get('/list_of_buyItem/:buyId',authCtrl.tokencheck,userAPI.getBuyItem);
router.get('/get_user_info/:uid',authCtrl.tokencheck,userAPI.getUserInfo);


module.exports= router;