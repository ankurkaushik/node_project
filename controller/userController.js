var userData = require('../model/userModel').userData;
var productDta = require('../model/productModel').productData;
var addCartDta = require('../model/addCartModel').addToCartSchema;
var buyProduct = require('../model/buyProduct').addToBuySchema;
var mongoQry = require('../mongo_query/mongoQuery');
const bcrypt = require('bcrypt');
var promise = require('promise')
var jwt = require('jsonwebtoken');
var multer = require('multer');
const assert = require('assert');
var mongoose = require('mongoose');
var buyIds;


const saltRounds = 10;


class userInformation {

      // userChecking,encryptPassword, userSaving are working on next() method working follow  define in routing
      userChecking(req, res, next) {
            var first_name = req.body.first_name;
            var last_name = req.body.last_name;
            var password = req.body.password;
            var email = req.body.email;
            var username = req.body.username;

            if (password !== '' && email !== '' && username !== '') {

                  userData.findOne({ email: req.body.email }, function (err, result) {
                        if (err) {
                              res.send({
                                    status: 400,
                                    err: err
                              })
                        } else {

                              if (result !== null) {
                                    res.status(2400).send({
                                          err: "user already registered"
                                    })
                              } else {
                                    next();
                              }
                        }
                  })

            } else {
                  res.status(400).send({
                        err: "check your field"
                  })
            }

      }
      encryptPassword(req, res, next) {
            bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                  req.body.encrty = hash;
                  next();
            });
      }
      userSaving(req, res) {

            var newUser = new userData({
                  firstName: req.body.first_name,
                  lastName: req.body.last_name,
                  userName: req.body.username,
                  email: req.body.email,
                  password: req.body.encrty
            })

            newUser.save().then((error, result) => {
                  if (error) {
                        res.status(400).send(error);
                  } else {

                        res.status(200).send({
                              msg: "user created !"
                        })
                  }
            })
      }


      //loginuserCheck,passwordCompare,assignJWT are working on next() method.
      loginuserCheck(req, res, next) {

            if (req.body.email !== '' && req.body.password !== '') {
                  userData.findOne({ email: req.body.email }, function (err, result) {

                        if (err) {
                              res.status(400).send(err);
                        } else {
                              if (result !== null) {
                                    req.body.userInfo = result;
                                    req.body.hashPassword = result.password;
                                    next();
                              } else {

                                    res.status(200).send({
                                          msg: 'pls create new account'
                                    })
                              }
                        }
                  })
            } else {
                  res.status(400).send({
                        msg: 'please fill all field'
                  })
            }
      }
      passwordCompare(req, res, next) {

            bcrypt.compare(req.body.password, req.body.hashPassword, function (err, result) {

                  if (result == true) {

                        next()
                  } else {
                        res.status(401).send({
                              msg: 'please enter correct password'
                        })
                  }

            });
      }
      assignJWT(req, res) {
            var token = jwt.sign("ankur", 'shhhhh');
            res.status(200).send({
                  "token": token,
                  "userInfo": req.body.userInfo
            });
      }


      //promise with async await for product saving 
      saveProduct = async function (req, res) {
            console.log("this is name of file", req.file)

            var valuechecker = () => {
                  return new Promise((resolve, reject) => {
                        if (req.body.product_barnd !== '') {
                              resolve(true);
                        } else {
                              reject({ err: "please fill all field" });
                        }
                  })
            }

            var productChecker = () => {
                  return new Promise((resolve, reject) => {
                        productDta.findOne({ productName: req.body.product_name }).then(result => {

                              if (result == null) {
                                    resolve(true)
                              } else {
                                    reject({ err: 'product already add' });
                              }
                        }).catch(error => {
                              reject(error);
                        })
                  })
            }



            var productCreate = () => {
                  var product = new productDta({
                        productName: req.body.product_name,
                        // BuyInfo : ObjectId(),
                        productQty: req.body.qty,
                        productBrand: req.body.product_brand,
                        productPrice: req.body.price,
                        ImageUrl: "uploads/" + req.file.originalname,
                        KeyPoint: {
                              keyPoint1: req.body.key1,
                              keyPoint2: req.body.key2,
                              keyPoint3: req.body.key3
                        }
                  })

                  // var error = product.validateSync();
                  product.save().then((result) => {
                        res.status(200).send({ msg: 'product created successfully !' });
                  }).catch((error) => {
                        res.send(error.message)
                  })
            }

            try {
                  var valuecheck = await valuechecker();
                  console.log('this is valuecheck', valuecheck);
                  if (valuecheck == true) {
                        await productChecker();
                        await productCreate();


                  }
            } catch (err) {

                  res.status(400).send(err);
            }


      }



      //This is api for Read product 
      getProductList(req, res) {
            console.log("this is get product")
            productDta.find({})
                  .then((result) => {
                        res.status(200).send(result);
                  })
                  .catch((error) => {
                        res.status(400).send({ err: error.massage });
                  })
      }

      /*
      this api for update the user profile it is using async await
      */

      updateUser = async function (req, res) {


            var checkUserValue = () => {

                  return new promise((resolve, reject) => {
                        if (req.body.first_name !== '' && req.body.last_name !== '' && req.body.username !== '' && req.body.password !== '') {

                              resolve(true)
                        } else {
                              reject({ errMsg: "please fill up all field" });
                        }
                  })
            }


            var ecrptyPass = () => {

                  return new promise((resolve, reject) => {
                        bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                              if (err) {
                                    reject({ errMsg: "something wong" })
                              }
                              resolve(hash);
                        });
                  })
            }

            var updateUserData = (password) => {

                  return new promise((resolve, reject) => {

                        userData.updateOne({ email: req.body.email },
                              {
                                    firstName: req.body.firstName,
                                    lastName: req.body.lastName,
                                    userName: req.body.userName,
                                    password: password

                              }, { upsert: true }).then((result) => {
                                    if (result.nModified == 1) {
                                          res.status(200).send({ msg: "your profile update successfully" })
                                    }
                              }).catch((error) => {
                                    reject(error);
                              })
                  })
            }



            try {
                  var checkUserFiled = await checkUserValue();
                  console.log("this is first resovle", checkUserFiled);
                  if (checkUserFiled == true) {
                        var newPassword = await ecrptyPass();
                        console.log("this is password", newPassword)
                        await updateUserData(newPassword)
                  }


            } catch (err) {
                  res.status(400).send(err);
            }
      }

      /*
       pagination api 
      */
      paging(req, res) {
            var pageno = req.params.pageNo;
            var query = productDta.find({}).skip(pageno * 2).limit(2);
            query.then((result) => {
                  res.status(200).send(result);
            }).catch((error) => {
                  res.status(500).send(error);
            })

      }
      /*
       there using regular Expression for searching value
      */

      GlobalSearch(req, res) {
            var query = productDta.find({
                  $or: [
                        { productName: { $regex: req.params.searchValue, $options: 'i' } }
                  ]
            })
            query.then((result) => {
                  res.status(200).send(result);
            }).catch((error) => {
                  res.status(500).send({ error })
            })
      }

      addToCart = async function (req, res) {


            var checkValueInCart = () => {

                  return new promise((resolve, reject) => {
                        addCartDta.findOne({ uid: req.params.id })
                              .then((result) => {

                                    if (result == null) {
                                          resolve(false);
                                    } else {
                                          resolve(true)
                                    }
                              }).catch((error) => {

                                    reject({ errMsg: 'some thing wrong' });
                              })
                  })

            }

            var addNewItem = () => {
                  return new promise((resolve, reject) => {
                        var cartValue = new addCartDta({
                              uid: req.params.id,
                              addToCart: req.body
                        })
                        cartValue.save()
                              .then((result) => {

                              }).catch((error) => {
                                    reject({ errMsg: "some thing wrong" });
                              })
                  })
            }

            var updateItem = () => {
                  return new promise((resolve, reject) => {
                        addCartDta.updateOne({ uid: req.params.id },
                              {
                                    $push: {
                                          addToCart: req.body

                                    }
                                    // addToCart : req.body
                              }, { upsert: true }).then((result) => {
                                    res.status(200).send({ msg: "successfully add into cart" })
                              }).catch((error) => {
                                    reject({ errMsg: "some thing wrong" });
                              })
                  })

            }

            try {
                  var checkValue = await checkValueInCart();

                  if (checkValue == false) {
                        var addNew = await addNewItem();
                  } else {
                        var replaceItem = await updateItem();
                  }


            } catch (err) {
                  res.status(400).send(err);
            }

      }


      getCartItem(req, res) {
            addCartDta.findOne((
                  { uid: req.params.id }
            )).then((result) => {
                  res.status(200).send(result);
            }).catch((error) => {
                  res.status(500).send(error);
            })
      }

      removeCart(req, res) {

            addCartDta.updateOne({ uid: req.params.id }, {
                  $pull: {
                        addToCart: req.body
                  }
            }, { upsert: true }).then((result) => {
                  res.status(200).send(true)
            }).catch((error) => {
                  res.status(400).send(false)
            })
      }

      getOneProduct(req, res) {
            productDta.findById({ _id: req.params.id })
                  .then((result) => {
                        res.status(200).send(result);
                  }).catch((error) => {
                        res.status(400).send(false);
                  })
      }


      startBuyNow = async function (req, res) {
            var checkBuyProductList = () => {
                  return new promise((resolve, reject) => {
                        buyProduct.findOne({ uid: req.params.uid })
                              .then((result) => {

                                    if (result == null) {
                                          resolve({
                                                value: true
                                          });
                                    } else {

                                          resolve({
                                                value: false,
                                                data: result
                                          });
                                    }
                              }).catch((error) => {
                                    reject({ errMsg: "some thing want wrong" })
                              })
                  })
            }

            var createBuyList = () => {
                  return new promise((resolve, reject) => {
                        var addBuyProduct = new buyProduct({
                              uid: req.params.uid,
                              addToBuy: req.params.pid
                        })
                        addBuyProduct.save()
                              .then((result) => {

                                    resolve(result._id);
                              }).catch((error) => {
                                    reject({ errMsg: "some thing want wrong" })
                              })
                  })
            }

            var updateProductInfo = (buyId) => {

                  return new promise((resolve, reject) => {
                        productDta.find(
                              {
                                    $and: [
                                          { _id: req.params.pid },
                                          { BuyInfoIds: buyId }
                                    ]
                              }
                        ).then((result) => {

                              if (result.length == 0) {
                                    productDta.update(
                                          { _id: req.params.pid },
                                          {
                                                $push: {
                                                      BuyInfoIds: buyId

                                                }
                                          }, { upsert: true }).then((result) => {


                                                resolve();
                                          }).catch((error) => {

                                                reject({ errMsg: "some thing want wrong" })
                                          })

                              } else {
                                    resolve()
                              }
                        }).catch((error) => {
                              reject({ errMsg: "some thing want wrong" })

                        })
                  })

                  /** this is query for populate for relation */
                  // productDta.findByIdAndUpdate({_id:req.params.pid},{BuyInfo:buyId}).then((result)=>{
                  //      resolve();
                  // }).catch((error) =>{
                  //       reject({errMsg:"some thing want wrong"})
                  // })

            }

            var updateBuyList = () => {
                  // console.log
                  return new promise((resolve, reject) => {
                        buyProduct.findOne({ addToBuy: req.params.pid }).then((result) => {

                              if (result == null) {
                                    buyProduct.update({ uid: req.params.uid },
                                          {
                                                $push: {
                                                      addToBuy: req.params.pid

                                                }
                                          }, { upsert: true })
                                          .then((result) => {

                                                resolve()
                                          }).catch((error) => {
                                                reject({ errMsg: "some thing want wrong" })
                                          })
                              } else {
                                    resolve();
                              }
                        }).catch((error) => {
                              reject({ errMsg: "some thing want wrong" })
                        })

                  })
            }

            var updateBuyInfoInUser = (BuyId) => {
                  return new promise((resolve, reject) => {
                        userData.find({
                              $and: [
                                    {
                                          _id: req.params.uid
                                    },
                                    {
                                          userBuyInformation: BuyId
                                    }
                              ]


                        }).then((result) => {

                              if (result.length == 0) {
                                    userData.update({ _id: req.params.uid },
                                          {
                                                $push: {
                                                      userBuyInformation: buyId
                                                }
                                          }, { upsert: true }).then((result) => {

                                                resolve();
                                          }).catch((error) => {

                                                reject();
                                          })
                              } else {
                                    resolve();
                              }
                        }).catch((error) => {

                        })

                  })
            }


            var getBuyList = () => {
                  return new promise((resolve, reject) => {
                        buyProduct.find({ uid: req.params.uid })
                              .then((result) => {
                                    res.status(200).send(result);
                              })
                  })
                  //      return new promise((resolve,reject) =>{
                  //            productDta.
                  //                 findById({_id:req.params.pid}).
                  //                 populate(
                  //                   'BuyInfo'
                  //                   ).
                  //                 then((result) =>{
                  //                       console.log("this is product info",result);
                  //                 }).catch((error) =>{
                  //                       console.log("this is error",error);
                  //                 })
                  //      })

            }


            try {
                  var buyId;
                  var checkBuyList = await checkBuyProductList();

                  if (checkBuyList.value == true) {

                        buyId = await createBuyList();

                        await updateProductInfo(buyId);
                        await updateBuyInfoInUser(buyId);
                  } else {

                        await updateBuyList();

                        await updateProductInfo(checkBuyList.data._id);
                        await updateBuyInfoInUser(checkBuyList.data._id);
                  }
                  ;
                  await getBuyList();


            } catch (error) {
                  res.status(400).send(error);
            }

      }


      getBuyItem(req, res) {
            console.log("this is ids", req.params.buyId)
            productDta.find({ BuyInfoIds: { $in: mongoose.Types.ObjectId(req.params.buyId) } })
                  .then((productList) => {
                        res.status(200).send(productList)
                  }).catch((error) => {
                        console.log("this is error", error)
                  })
      }

      getUserInfo(req,res){
             console.log("this is userId",req.params.uid);
             userData.findById({_id:req.params.uid}).then((result)=>{
                   console.log("thsi is user result",result);
                   res.status(200).send(result);
             }).catch((error) =>{
                   res.status(400).send({errMsg:"some thing went wrong"});
             })
      }

}


module.exports = userInformation