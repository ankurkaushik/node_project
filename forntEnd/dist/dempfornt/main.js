(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/login/login.component.css":
/*!***************************************************!*\
  !*** ./src/app/account/login/login.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/login/login.component.html":
/*!****************************************************!*\
  !*** ./src/app/account/login/login.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Login User</h4>\n<div class=\"container\">\n  <div class=\"row\">\n      <div class=\"col-md-6 offset-md-3\">\n\n<form (submit)=\"loginUser() \">\n  Email Id  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"login_user.email\" name=\"email\"><br>\n  Password  <input type=\"password\"  class=\"form-control\" [(ngModel)]=\"login_user.password\" name=\"password\"><br>\n  <input type=\"submit\" name=\"login\">\n</form>\n      </div>\n      </div>\n    </div>"

/***/ }),

/***/ "./src/app/account/login/login.component.ts":
/*!**************************************************!*\
  !*** ./src/app/account/login/login.component.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var basic_service_service_1 = __webpack_require__(/*! ../../service/basic-service.service */ "./src/app/service/basic-service.service.ts");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(basicService, route) {
        this.basicService = basicService;
        this.route = route;
        this.login_user = {
            email: '',
            password: ''
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.loginUser = function () {
        var _this = this;
        console.log("this is login user", this.login_user);
        this.basicService.loginUser(this.login_user).subscribe(function (data) {
            if (data.token) {
                console.log(data);
                localStorage.setItem('token', data.token);
                localStorage.setItem('username', data.userInfo.userName);
                localStorage.setItem('user_id', data.userInfo._id);
                _this.basicService.dataAfterLogin(data.userInfo);
                _this.route.navigateByUrl('');
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/account/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/account/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [basic_service_service_1.BasicServiceService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "./src/app/account/sign-up/sign-up.component.css":
/*!*******************************************************!*\
  !*** ./src/app/account/sign-up/sign-up.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/sign-up/sign-up.component.html":
/*!********************************************************!*\
  !*** ./src/app/account/sign-up/sign-up.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>User signUp</h3>\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-6 offset-md-3\">\n          {{errMsg}}\n          <form  (submit)='signUpUser()'>\n  First Name<input type=\"text\" class=\"form-control\" [(ngModel)]=\"user.first_name\" name=\"first_name\"><br>\n  Last Name <input type=\"text\" class=\"form-control\" [(ngModel)]= \"user.last_name\" name=\"last_name\"><br>\n  UserName <input type=\"text\" class=\"form-control\" [(ngModel)]= \"user.username\" name=\"username\"><br>\n  Email Id <input type=\"text\" class=\"form-control\" [(ngModel)]= \"user.email\" name=\"email\"><br>\n  Password  <input type=\"password\" class=\"form-control\"  [(ngModel)]= \"user.password\" name=\"password\"><br>\n  <input type=\"submit\">\n</form>\n\n</div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/account/sign-up/sign-up.component.ts":
/*!******************************************************!*\
  !*** ./src/app/account/sign-up/sign-up.component.ts ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var basic_service_service_1 = __webpack_require__(/*! ../../service/basic-service.service */ "./src/app/service/basic-service.service.ts");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(basicService, route
    // private formBuilder: FormBuilder
    ) {
        //  this.userForm = this.formBuilder.group({
        //     first_name : ['', Validators.required],
        //     last_name : ['', Validators.required],
        //     email : ['', Validators.required],
        //     username : ['', Validators.required],
        //     password : ['', Validators.required]
        this.basicService = basicService;
        this.route = route;
        this.user = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            username: '',
        };
        //  })
    }
    SignUpComponent.prototype.ngOnInit = function () {
    };
    SignUpComponent.prototype.signUpUser = function () {
        var _this = this;
        this.basicService.createUser(this.user).subscribe(function (data) {
            if (data.err) {
                _this.errMsg = data.err;
            }
            else {
                var userloginInfo = {
                    email: _this.user.email,
                    password: _this.user.password
                };
                _this.basicService.loginUser(userloginInfo).subscribe(function (data) {
                    console.log('this is data', data);
                    if (data.token) {
                        localStorage.setItem('token', data.token);
                        localStorage.setItem('username', _this.user.username);
                        _this.route.navigateByUrl('');
                    }
                });
            }
        });
    };
    SignUpComponent = __decorate([
        core_1.Component({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/account/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.css */ "./src/app/account/sign-up/sign-up.component.css")]
        }),
        __metadata("design:paramtypes", [basic_service_service_1.BasicServiceService,
            router_1.Router
            // private formBuilder: FormBuilder
        ])
    ], SignUpComponent);
    return SignUpComponent;
}());
exports.SignUpComponent = SignUpComponent;


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div style=\"margin-top: 5%\">\n<router-outlet></router-outlet>\n</div>\n<!-- <app-footer></app-footer> -->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var app_component_1 = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var header_component_1 = __webpack_require__(/*! ./layout/header/header.component */ "./src/app/layout/header/header.component.ts");
var footer_component_1 = __webpack_require__(/*! ./layout/footer/footer.component */ "./src/app/layout/footer/footer.component.ts");
var mainpage_component_1 = __webpack_require__(/*! ../app/home/mainpage/mainpage.component */ "./src/app/home/mainpage/mainpage.component.ts");
var sign_up_component_1 = __webpack_require__(/*! ../app/account/sign-up/sign-up.component */ "./src/app/account/sign-up/sign-up.component.ts");
var login_component_1 = __webpack_require__(/*! ../app/account/login/login.component */ "./src/app/account/login/login.component.ts");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
// import { Http } from '@angular/http';
var http_1 = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var ng_simple_slideshow_1 = __webpack_require__(/*! ng-simple-slideshow */ "./node_modules/ng-simple-slideshow/ng-simple-slideshow.es5.js");
var sidenav_component_1 = __webpack_require__(/*! ./layout/sidenav/sidenav.component */ "./src/app/layout/sidenav/sidenav.component.ts");
var ngx_pagination_1 = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
var serachproduct_component_1 = __webpack_require__(/*! ./home/serachproduct/serachproduct.component */ "./src/app/home/serachproduct/serachproduct.component.ts");
var view_cart_component_1 = __webpack_require__(/*! ./home/view-cart/view-cart.component */ "./src/app/home/view-cart/view-cart.component.ts");
var product_details_component_1 = __webpack_require__(/*! ./home/product-details/product-details.component */ "./src/app/home/product-details/product-details.component.ts");
var routes = [
    {
        path: '',
        component: mainpage_component_1.MainpageComponent,
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'signup',
        component: sign_up_component_1.SignUpComponent
    },
    {
        path: 'search_product',
        component: serachproduct_component_1.SerachproductComponent
    },
    {
        path: 'view_cart',
        component: view_cart_component_1.ViewCartComponent
    },
    {
        path: 'product_details/:id',
        component: product_details_component_1.ProductDetailsComponent
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                header_component_1.HeaderComponent,
                footer_component_1.FooterComponent,
                mainpage_component_1.MainpageComponent,
                sign_up_component_1.SignUpComponent,
                login_component_1.LoginComponent,
                sidenav_component_1.SidenavComponent,
                serachproduct_component_1.SerachproductComponent,
                view_cart_component_1.ViewCartComponent,
                product_details_component_1.ProductDetailsComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule.forRoot(routes),
                // Http
                http_1.HttpModule,
                forms_1.FormsModule,
                common_1.CommonModule,
                forms_1.ReactiveFormsModule,
                ng_simple_slideshow_1.SlideshowModule,
                ngx_pagination_1.NgxPaginationModule
            ],
            exports: [
                router_1.RouterModule
            ],
            providers: [],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/home/mainpage/mainpage.component.css":
/*!******************************************************!*\
  !*** ./src/app/home/mainpage/mainpage.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\n    list-style-type: none;\n    background: antiquewhite;\n    width: 390%;\n    height: 121px;\n    margin-left: 5%;\n  }\n#priceStyle\n  {\n    margin-left: 10%;\n  }\n#styleCart{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n  }\n#styleBuy{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n    margin-top: 3%;\n  }\nbutton:hover {\n    opacity: 0.7;\n  }\n.ngx-pagination {\n    margin-left: 294% !important;\n    margin-bottom: 1rem;\n    width: 104% !important;\n  }"

/***/ }),

/***/ "./src/app/home/mainpage/mainpage.component.html":
/*!*******************************************************!*\
  !*** ./src/app/home/mainpage/mainpage.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sidenav></app-sidenav>\n<div class=\"w3-main\" style=\"margin-left:250px\">\n        <div class=\"w3-hide-large\" style=\"margin-top:83px\"></div>\n<div class=\"w3-display-container w3-container\">\n        <img src=\"../../../assets/image/jeans.jpg\" alt=\"Jeans\" style=\"width:100%\">\n        <div class=\"w3-display-topleft w3-text-white\" style=\"padding:24px 48px\">\n          <h1 class=\"w3-jumbo w3-hide-small\">New arrivals</h1>\n          <h1 class=\"w3-hide-large w3-hide-medium\">New arrivals</h1>\n          <h1 class=\"w3-hide-small\">COLLECTION 2016</h1>\n          <p><a href=\"#jeans\" class=\"w3-button w3-black w3-padding-large w3-large\">SHOP NOW</a></p>\n        </div>\n      </div>\n    \n      <div class=\"w3-container w3-text-grey\" id=\"jeans\">\n        <p>Home</p>\n      </div>\n\n      <div class=\"w3-row w3-grayscale\">\n            <div class=\"w3-col l3 s6\">\n                <ul  *ngFor=\"let product of productArry | paginate: { itemsPerPage: 4, currentPage: p }\">\n                    <li>\n                        <img src=\"{{url+product.ImageUrl}}\" style=\"height: 100px;     margin-top: 1%;\">\n                       \n                     \n                    </li> \n                    <li style=\"margin-left: 10%;\n                    margin-top: -9%;\">\n                        Product Name - {{product.productName}}\n                    </li>\n                    <li style=\"    margin-left: 10%;\n                    \">\n                       Product Brand - {{product.productBrand}}\n                    </li> \n                    <li id=\"priceStyle\">\n                      Product price - {{product.productPrice}}\n                    </li>\n                    <h6 style=\"    margin-left: 40%;\n                    margin-top: -7%;\">\n                        key feature\n                        <li> \n                           - {{product.KeyPoint.keyPoint1|| ''}}\n                        </li>\n                        <li> \n                                - {{product.KeyPoint.keyPoint2}}\n                         </li>\n                         <li> \n                               - {{product.KeyPoint.keyPoint3}}\n                        </li>\n                    </h6>\n                    <li style=\"    margin-left: 78%;\n                    margin-top: -9%;\n                \">\n                    <button id=\"styleCart\" (click)=\"addToCart(product)\">Add to Cart</button>\n                    <button id=\"styleBuy\" routerLink=\"product_details/{{product._id}}\">Details</button>\n                    </li>\n                </ul>\n               <div style=\"    margin-left: 303%;\n               width: 105%;\"> <pagination-controls (pageChange)=\"p = $event\"></pagination-controls></div>\n \n              </div>\n      </div>"

/***/ }),

/***/ "./src/app/home/mainpage/mainpage.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/home/mainpage/mainpage.component.ts ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var productinfo_service_1 = __webpack_require__(/*! ../../service/productinfo.service */ "./src/app/service/productinfo.service.ts");
var add_cart_service_1 = __webpack_require__(/*! ../../service/add-cart.service */ "./src/app/service/add-cart.service.ts");
var environment_1 = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var MainpageComponent = /** @class */ (function () {
    function MainpageComponent(prodService, addCartSer) {
        this.prodService = prodService;
        this.addCartSer = addCartSer;
        this.productArry = [];
        this.p = 1;
        this.url = environment_1.environment.baseurl;
    }
    MainpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.prodService.getProduct().subscribe(function (data) {
            _this.productArry = data;
        });
    };
    MainpageComponent.prototype.addToCart = function (value) {
        this.addCartSer.addProduct(value);
    };
    MainpageComponent = __decorate([
        core_1.Component({
            selector: 'app-mainpage',
            template: __webpack_require__(/*! ./mainpage.component.html */ "./src/app/home/mainpage/mainpage.component.html"),
            styles: [__webpack_require__(/*! ./mainpage.component.css */ "./src/app/home/mainpage/mainpage.component.css")]
        }),
        __metadata("design:paramtypes", [productinfo_service_1.ProductinfoService,
            add_cart_service_1.AddCartService])
    ], MainpageComponent);
    return MainpageComponent;
}());
exports.MainpageComponent = MainpageComponent;


/***/ }),

/***/ "./src/app/home/product-details/product-details.component.css":
/*!********************************************************************!*\
  !*** ./src/app/home/product-details/product-details.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".attr,.attr2{cursor:pointer;margin-right:5px;height:20px;font-size:10px;padding:2px;border:1px solid gray;border-radius:2px;}\n#styleCart{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n  }\n#styleBuy{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n    margin-top: 3%;\n  }\nbutton:hover {\n    opacity: 0.7;\n  }"

/***/ }),

/***/ "./src/app/home/product-details/product-details.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/home/product-details/product-details.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sidenav></app-sidenav>\n<div class=\"container\">\n  <div class=\"row\">\n      <div class=\"col-sm-4\">\n\n      </div>  \n      <div class=\"col-sm-4\">\n         <img src=\"{{url+productInfo.ImageUrl}}\">\n            \n     </div>\n     <div class=\"col-xs-4\">\n        <div class=\"row\">\n            <h4>Product Details</h4>\n            </div> \n           <div class=\"row\">\n           <h4>Product Name - {{productInfo.productName}}</h4>\n           </div>\n           <div class=\"row\">\n            <h4>Product Barnd -{{productInfo.productBrand}}</h4>\n           </div>\n           <div class=\"row\">\n              <h6 class=\"title-attr\" style=\"margin-top:15px;\" ><small>COLOR</small></h6>                    \n              <div>\n                  <div class=\"attr\" style=\"width:25px;background:#5a5a5a; margin-top: 15px;\"></div>\n                  <div class=\"attr\" style=\"width:25px;background:white; margin-left: 34px;\n                  margin-top: -20px;\"></div>\n              </div>\n          </div>\n\n          <div class=\"row\">\n              <h5>Features :- </h5><br>\n                 {{productInfo.KeyPoint.keyPoint1}} <br>\n                 {{productInfo.KeyPoint.keyPoint2}} <br>\n                 {{productInfo.KeyPoint.keyPoint3}}\n          </div>\n          <div class=\"row\">\n              <h5>Product Price - {{productInfo.productPrice}}</h5>\n              </div>\n              <div class=\"row\">\n                  <div class=\"btn-minus\">-</div>\n                  <input value=\"1\" style=\"width: 20%;\"/>\n                  <div class=\"btn-plus\">+</div>\n              </div><br>\n              <div class=\"row\">\n                  <button id=\"styleCart\">ADD CART</button>\n                  <button id=\"styleBuy\">BUY NOW</button>\n                  \n              </div>\n             \n     </div>\n\n  </div>\n  <div class=\"row\">\n    \n      <div class=\"col-sm-4\">\n      </div>  \n      <div class=\"col-sm-4\">\n\n            details\n     </div>\n  </div>\n\n  <div>\n\n  </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/home/product-details/product-details.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/home/product-details/product-details.component.ts ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var productinfo_service_1 = __webpack_require__(/*! ../../service/productinfo.service */ "./src/app/service/productinfo.service.ts");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var environment_1 = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var ProductDetailsComponent = /** @class */ (function () {
    function ProductDetailsComponent(route, prodService) {
        var _this = this;
        this.route = route;
        this.prodService = prodService;
        this.url = environment_1.environment.baseurl;
        route.params.subscribe(function (params) {
            _this.pid = params['id'];
            console.log("this is proddcut id", _this.pid);
        });
    }
    ProductDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.prodService.getParticularProduct(this.pid).subscribe(function (data) {
            console.log(data);
            _this.productInfo = data;
        });
    };
    ProductDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-product-details',
            template: __webpack_require__(/*! ./product-details.component.html */ "./src/app/home/product-details/product-details.component.html"),
            styles: [__webpack_require__(/*! ./product-details.component.css */ "./src/app/home/product-details/product-details.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, productinfo_service_1.ProductinfoService])
    ], ProductDetailsComponent);
    return ProductDetailsComponent;
}());
exports.ProductDetailsComponent = ProductDetailsComponent;


/***/ }),

/***/ "./src/app/home/serachproduct/serachproduct.component.css":
/*!****************************************************************!*\
  !*** ./src/app/home/serachproduct/serachproduct.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\n    list-style-type: none;\n    background: antiquewhite;\n    width: 390%;\n    height: 121px;\n    margin-left: 5%;\n  }\n#priceStyle\n  {\n    margin-left: 10%;\n  }\n#styleCart{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n  }\n#styleBuy{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n    margin-top: 3%;\n  }\nbutton:hover {\n    opacity: 0.7;\n  }\n.ngx-pagination {\n    margin-left: 294% !important;\n    margin-bottom: 1rem;\n    width: 104% !important;\n  }"

/***/ }),

/***/ "./src/app/home/serachproduct/serachproduct.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/home/serachproduct/serachproduct.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sidenav></app-sidenav>\n<div class=\"w3-main\" style=\"margin-left:250px\">\n  <div class=\"w3-row w3-grayscale\">\n    <div class=\"w3-col l3 s6\" *ngIf=\"showSearch == false\">\n        <ul  *ngFor=\"let product of productArry | paginate: { itemsPerPage: 4, currentPage: p }\">\n            <li>\n                <img src=\"{{url+product.ImageUrl}}\" style=\"height: 100px;     margin-top: 1%;\">\n               \n             \n            </li> \n            <li style=\"margin-left: 10%;\n            margin-top: -9%;\">\n                Product Name - {{product.productName}}\n            </li>\n            <li style=\"    margin-left: 10%;\n            \">\n               Product Brand - {{product.productBrand}}\n            </li> \n            <li id=\"priceStyle\">\n              Product price - {{product.productPrice}}\n            </li>\n            <h6 style=\"    margin-left: 40%;\n            margin-top: -7%;\">\n                key feature\n                <li> \n                   - {{product.KeyPoint.keyPoint1|| ''}}\n                </li>\n                <li> \n                        - {{product.KeyPoint.keyPoint2}}\n                 </li>\n                 <li> \n                       - {{product.KeyPoint.keyPoint3}}\n                </li>\n            </h6>\n            <li style=\"    margin-left: 78%;\n            margin-top: -9%;\n        \">\n            <button id=\"styleCart\">Add to Cart</button>\n            <button id=\"styleBuy\">Details</button>\n            </li>\n        </ul>\n       <div style=\"    margin-left: 303%;\n       width: 105%;\"> <pagination-controls (pageChange)=\"p = $event\"></pagination-controls></div>\n\n      </div>\n      <div *ngIf=\"showSearch == true\">\n            NO Product Found\n      </div>\n\n</div>"

/***/ }),

/***/ "./src/app/home/serachproduct/serachproduct.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/home/serachproduct/serachproduct.component.ts ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var productinfo_service_1 = __webpack_require__(/*! ../../service/productinfo.service */ "./src/app/service/productinfo.service.ts");
var SerachproductComponent = /** @class */ (function () {
    function SerachproductComponent(prodService) {
        this.prodService = prodService;
        this.productArry = [];
        this.showSearch = true;
        this.url = "http://192.168.1.43:4000/";
    }
    SerachproductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.prodService.searchValue.subscribe(function (val) {
            console.log("value on", val);
            if (val.length == 0) {
                _this.showSearch = true;
            }
            else {
                _this.prodService.getSearchProduct(val).subscribe(function (data) {
                    console.log(data);
                    if (data.length == 0) {
                        _this.showSearch = true;
                    }
                    else {
                        _this.showSearch = false;
                        _this.productArry = data;
                    }
                });
            }
        });
    };
    SerachproductComponent = __decorate([
        core_1.Component({
            selector: 'app-serachproduct',
            template: __webpack_require__(/*! ./serachproduct.component.html */ "./src/app/home/serachproduct/serachproduct.component.html"),
            styles: [__webpack_require__(/*! ./serachproduct.component.css */ "./src/app/home/serachproduct/serachproduct.component.css")]
        }),
        __metadata("design:paramtypes", [productinfo_service_1.ProductinfoService])
    ], SerachproductComponent);
    return SerachproductComponent;
}());
exports.SerachproductComponent = SerachproductComponent;


/***/ }),

/***/ "./src/app/home/view-cart/view-cart.component.css":
/*!********************************************************!*\
  !*** ./src/app/home/view-cart/view-cart.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\n    list-style-type: none;\n    background: antiquewhite;\n    width: 390%;\n    height: 129px;\n    margin-left: 5%;\n  }\n#priceStyle\n  {\n    margin-left: 10%;\n  }\n#styleCart{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n  }\n#styleBuy{\n    border: none;\n    outline: 0;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 90%;\n    font-size: 14px;\n    margin-top: 3%;\n  }\nbutton:hover {\n    opacity: 0.7;\n  }\n.ngx-pagination {\n    margin-left: 294% !important;\n    margin-bottom: 1rem;\n    width: 104% !important;\n  }"

/***/ }),

/***/ "./src/app/home/view-cart/view-cart.component.html":
/*!*********************************************************!*\
  !*** ./src/app/home/view-cart/view-cart.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sidenav></app-sidenav>\n\n\n\n<app-sidenav></app-sidenav>\n<div class=\"w3-main\" style=\"margin-left:250px\">\n  <div class=\"w3-row w3-grayscale\">\n    <div class=\"w3-col l3 s6\" *ngIf=\"AllCartItem.length !== 0\">\n        <ul  *ngFor=\"let product of AllCartItem | paginate: { itemsPerPage: 4, currentPage: p }\">\n            <li>\n                <img src=\"{{url+product.ImageUrl}}\" style=\"height: 100px;     margin-top: 1%;\">\n               \n             \n            </li> \n            <li style=\"margin-left: 10%;\n            margin-top: -9%;\">\n                Product Name - {{product.productName}}\n            </li>\n            <li style=\"    margin-left: 10%;\n            \">\n               Product Brand - {{product.productBrand}}\n            </li> \n            <li id=\"priceStyle\">\n              Product price - {{product.productPrice}}\n            </li>\n            <h6 style=\"    margin-left: 40%;\n            margin-top: -7%;\">\n                key feature\n                <li> \n                   - {{product.KeyPoint.keyPoint1|| ''}}\n                </li>\n                <li> \n                        - {{product.KeyPoint.keyPoint2}}\n                 </li>\n                 <li> \n                       - {{product.KeyPoint.keyPoint3}}\n                </li>\n            </h6>\n            <p style=\"margin-left: 62%;\n            margin-top: -6%;\">\n              <button (click)=\"dev()\">-</button>\n               {{productTotal}}\n               <button (click)= \"inc()\">+</button>\n\n\n            </p>\n            <li style=\"    margin-left: 78%;\n            margin-top: -9%;\">\n            <button id=\"styleCart\">Buy Now</button>\n            <button id=\"styleBuy\">Details</button>\n            <button id=\"styleBuy\" (click) =\"removeToCart(product)\">Remove To Cart</button>\n            </li>\n        </ul>\n       <div style=\"    margin-left: 303%;\n       width: 105%;\"> <pagination-controls (pageChange)=\"p = $event\"></pagination-controls></div>\n\n      </div>\n      <div *ngIf=\" AllCartItem.length== 0\">\n            NO Product Found\n      </div>\n\n</div>"

/***/ }),

/***/ "./src/app/home/view-cart/view-cart.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/home/view-cart/view-cart.component.ts ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var add_cart_service_1 = __webpack_require__(/*! ../../service/add-cart.service */ "./src/app/service/add-cart.service.ts");
var environment_1 = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var ViewCartComponent = /** @class */ (function () {
    function ViewCartComponent(addCartSer) {
        this.addCartSer = addCartSer;
        this.AllCartItem = [];
        this.productTotal = 1;
        this.url = environment_1.environment.baseurl;
    }
    ViewCartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userId = localStorage.getItem('user_id');
        this.addCartSer.getCartItem(this.userId).subscribe(function (data) {
            _this.AllCartItem = data.addToCart;
        });
    };
    ViewCartComponent.prototype.inc = function () {
        this.productTotal = this.productTotal + 1;
    };
    ViewCartComponent.prototype.dec = function () {
        this.productTotal = this.productTotal - 1;
    };
    ViewCartComponent.prototype.removeToCart = function (product) {
        var _this = this;
        this.addCartSer.removeItemFormCart(this.userId, product).subscribe(function (data) {
            if (data == true) {
                _this.userId = localStorage.getItem('user_id');
                _this.addCartSer.getCartItem(_this.userId).subscribe(function (data) {
                    _this.AllCartItem = data.addToCart;
                });
            }
            else {
            }
        });
    };
    ViewCartComponent = __decorate([
        core_1.Component({
            selector: 'app-view-cart',
            template: __webpack_require__(/*! ./view-cart.component.html */ "./src/app/home/view-cart/view-cart.component.html"),
            styles: [__webpack_require__(/*! ./view-cart.component.css */ "./src/app/home/view-cart/view-cart.component.css")]
        }),
        __metadata("design:paramtypes", [add_cart_service_1.AddCartService])
    ], ViewCartComponent);
    return ViewCartComponent;
}());
exports.ViewCartComponent = ViewCartComponent;


/***/ }),

/***/ "./src/app/layout/footer/footer.component.css":
/*!****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/footer/footer.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/footer/footer.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark justify-content-end fixed-bottom\">\n  <h6 style=\"color:azure;\">copyright@2019 </h6>\n\n</nav>\n"

/***/ }),

/***/ "./src/app/layout/footer/footer.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/footer/footer.component.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/layout/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/layout/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;


/***/ }),

/***/ "./src/app/layout/header/header.component.css":
/*!****************************************************!*\
  !*** ./src/app/layout/header/header.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/header/header.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/header/header.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <a routerLink=\"login\">Login</a>\n<a routerLink =\"signup\">signupa</a> -->\n<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark justify-content-end fixed-top\" *ngIf=\"showMenu == false\">\n  <!-- Brand -->\n\n  <!-- Links -->\n  <ul class=\"navbar-nav\">\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" routerLink=''>Home</a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" routerLink=\"login\">Login</a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" routerLink =\"signup\">SignUp</a>\n    </li>\n   \n    <!-- Dropdown -->\n   \n  </ul>\n</nav>\n\n<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark justify-content-end fixed-top\" *ngIf=\"showMenu == true\">\n    <!-- Brand -->\n  \n    <!-- Links -->\n    <ul class=\"navbar-nav\">\n      <input type=\"text\"  [(ngModel)]=\"searchInput\">\n          <button class=\"btn btn-default\" style=\"background-color:white;\" (click)=\"search()\">\n            search\n          </button>\n          \n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=''>Home</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" ><img src=\"../../../assets/image/shopping-cart.png\" style=\"height: 20px;\" routerLink='view_cart'>\n          <span class=\"badge badge-pill badge-info\">{{addToCartCount}}</span>\n        </a>\n      </li>\n     \n      <!-- Dropdown -->\n      <li class=\"nav-item dropdown\">\n        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\">\n          {{userInfo}}\n        </a>\n        <div class=\"dropdown-menu\">\n          <a class=\"dropdown-item\" href=\"#\">profile</a>\n          <a class=\"dropdown-item\" href=\"#\">Order</a>\n          <a class=\"dropdown-item\" (click)=\"Logout()\">Log out</a>\n        </div>\n      </li>\n     \n    </ul>\n  </nav>"

/***/ }),

/***/ "./src/app/layout/header/header.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/header/header.component.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var basic_service_service_1 = __webpack_require__(/*! ../../service/basic-service.service */ "./src/app/service/basic-service.service.ts");
var productinfo_service_1 = __webpack_require__(/*! ../../service/productinfo.service */ "./src/app/service/productinfo.service.ts");
var router_1 = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var add_cart_service_1 = __webpack_require__(/*! ../../service/add-cart.service */ "./src/app/service/add-cart.service.ts");
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(basicService, prodService, route, addCartSer) {
        this.basicService = basicService;
        this.prodService = prodService;
        this.route = route;
        this.addCartSer = addCartSer;
        this.addToCartCount = 0;
        this.newArryValue = [];
        this.pushCart = [];
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.basicService.userDataSource.subscribe(function (val) {
            if (val.length !== 0) {
                console.log("this is value", Object.values(val));
                var newUserInfo = Object.values(val);
                _this.showMenu = true;
                _this.userInfo = newUserInfo[6];
                var userId_1 = localStorage.getItem('user_id');
                console.log(userId_1);
                _this.addCartSer.getCartItem(userId_1).subscribe(function (data) {
                    console.log("strting cart value", data.addToCart);
                    _this.countCartAPi = data.addToCart.length;
                    // this.newArryValue.push(data.addToCart);
                    // console.log(this.countCartAPi);
                    _this.addToCartCount = _this.countCartAPi;
                });
            }
            else {
                console.log("no");
                var userinfo = localStorage.getItem('username');
                console.log(userinfo);
                _this.userInfo = userinfo;
                if (userinfo) {
                    _this.showMenu = true;
                }
                else {
                    _this.showMenu = false;
                }
            }
        });
        console.log("this is testing");
        var userId = localStorage.getItem('user_id');
        this.addCartSer.getCartItem(userId).subscribe(function (data) {
            console.log("strting cart value", data.addToCart);
            _this.countCartAPi = data.addToCart.length;
            // this.newArryValue.push(data.addToCart);
            // console.log(this.countCartAPi);
            _this.addToCartCount = _this.countCartAPi;
        });
        this.addCartSer.addCart.subscribe(function (val) {
            console.log("this is value", val);
            //  this.pushCart.push(val);
            //  console.log(this.pushCart.length);
            //  this.addToCartCount = this.pushCart.length;
            if (val.length == 1) {
                console.log('no one');
                _this.addToCartCount = 0;
                _this.pushCart.length = 0;
                //  console.log("no value",this.countCartAPi);
                //  this.addToCartCount =this.countCartAPi
            }
            else {
                _this.pushCart.push(val);
                console.log(_this.pushCart.length);
                // this.addToCartCount = this.pushCart.length;
                _this.addToCartCount = _this.countCartAPi + _this.pushCart.length - 1;
                // this.newArryValue[0].push(val);
                // console.log("add to card api",this.newArryValue);
                var userId_2 = localStorage.getItem('user_id');
                _this.addCartSer.saveAddCartData(userId_2, val).subscribe(function (data) {
                });
            }
        });
        // let userId = localStorage.getItem('user_id');
        // this.addCartSer.getCartItem(userId).subscribe((data)=>{
        // })
    };
    HeaderComponent.prototype.Logout = function () {
        localStorage.removeItem('username');
        localStorage.removeItem('token');
        localStorage.removeItem('user_id');
        this.showMenu = false;
    };
    HeaderComponent.prototype.search = function () {
        console.log("search", this.searchInput);
        this.prodService.globalSearch(this.searchInput);
        this.route.navigateByUrl('/search_product');
    };
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/layout/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/layout/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [basic_service_service_1.BasicServiceService,
            productinfo_service_1.ProductinfoService,
            router_1.Router,
            add_cart_service_1.AddCartService])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;


/***/ }),

/***/ "./src/app/layout/sidenav/sidenav.component.css":
/*!******************************************************!*\
  !*** ./src/app/layout/sidenav/sidenav.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/layout/sidenav/sidenav.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/sidenav/sidenav.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<nav class=\"w3-sidebar w3-bar-block w3-white w3-collapse w3-top\" style=\"z-index:3;width:250px\" id=\"mySidebar\">\n    <div class=\"w3-padding-64 w3-large w3-text-grey\" style=\"font-weight:bold\">\n        <h3 class=\"w3-wide\"><b>LOGO</b></h3>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Shirts</a>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Dresses</a>\n      <div id=\"demoAcc\" class=\"w3-bar-block w3-hide w3-padding-large w3-medium\">\n        <a href=\"#\" class=\"w3-bar-item w3-button w3-light-grey\"><i class=\"fa fa-caret-right w3-margin-right\"></i>Skinny</a>\n        <a href=\"#\" class=\"w3-bar-item w3-button\">Relaxed</a>\n       \n      </div>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Jackets</a>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Gymwear</a>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Blazers</a>\n      <a href=\"#\" class=\"w3-bar-item w3-button\">Shoes</a>\n    </div>\n    <a href=\"#footer\" class=\"w3-bar-item w3-button w3-padding\">Contact</a> \n    <a href=\"javascript:void(0)\" class=\"w3-bar-item w3-button w3-padding\" onclick=\"document.getElementById('newsletter').style.display='block'\">Newsletter</a> \n    <a href=\"#footer\"  class=\"w3-bar-item w3-button w3-padding\">Subscribe</a>\n  </nav>\n"

/***/ }),

/***/ "./src/app/layout/sidenav/sidenav.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/sidenav/sidenav.component.ts ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var SidenavComponent = /** @class */ (function () {
    function SidenavComponent() {
    }
    SidenavComponent.prototype.ngOnInit = function () {
    };
    SidenavComponent = __decorate([
        core_1.Component({
            selector: 'app-sidenav',
            template: __webpack_require__(/*! ./sidenav.component.html */ "./src/app/layout/sidenav/sidenav.component.html"),
            styles: [__webpack_require__(/*! ./sidenav.component.css */ "./src/app/layout/sidenav/sidenav.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidenavComponent);
    return SidenavComponent;
}());
exports.SidenavComponent = SidenavComponent;


/***/ }),

/***/ "./src/app/service/add-cart.service.ts":
/*!*********************************************!*\
  !*** ./src/app/service/add-cart.service.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var rxjs_1 = __webpack_require__(/*! ../../../node_modules/rxjs */ "./node_modules/rxjs/_esm5/index.js");
var http_1 = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var operators_1 = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var rxjs_2 = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var environment_1 = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var AddCartService = /** @class */ (function () {
    function AddCartService(http) {
        this.http = http;
        this.addCart = new rxjs_1.BehaviorSubject([]);
        this.url = environment_1.environment.serverUrl;
        // this.token =localStorage.getItem('token');
        // console.log("token",this.token);
    }
    AddCartService.prototype.createAuthorizationHeader = function (headers) {
        this.token = localStorage.getItem('token');
        console.log("token", this.token);
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        headers.append("token", this.token);
        return new http_1.RequestOptions({ headers: headers });
    };
    AddCartService.prototype.addProduct = function (value) {
        // this.addCart.next(this.addCart.getValue().concat([value])); //this is for making array
        this.addCart.next(value);
    };
    AddCartService.prototype.saveAddCartData = function (id, obj) {
        console.log("this is object", obj);
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.post(this.url + "/add_to_cart/" + id, obj, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_2.of(err.json());
        }));
    };
    AddCartService.prototype.getCartItem = function (id) {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.get(this.url + '/get_cart_item/' + id, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_2.of(err.json());
        }));
    };
    AddCartService.prototype.removeItemFormCart = function (Id, obj) {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.post(this.url + "/remove_to_cart/" + Id, obj, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_2.of(err.json());
        }));
    };
    ;
    AddCartService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], AddCartService);
    return AddCartService;
}());
exports.AddCartService = AddCartService;


/***/ }),

/***/ "./src/app/service/basic-service.service.ts":
/*!**************************************************!*\
  !*** ./src/app/service/basic-service.service.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var environment_1 = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var http_1 = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var operators_1 = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var rxjs_2 = __webpack_require__(/*! ../../../node_modules/rxjs */ "./node_modules/rxjs/_esm5/index.js");
var BasicServiceService = /** @class */ (function () {
    function BasicServiceService(http) {
        this.http = http;
        this.url = environment_1.environment.serverUrl;
        this.userDataSource = new rxjs_2.BehaviorSubject([]);
    }
    BasicServiceService.prototype.createAuthorizationHeader = function (headers) {
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        return new http_1.RequestOptions({ headers: headers });
    };
    BasicServiceService.prototype.dataAfterLogin = function (data) {
        this.userDataSource.next(data);
    };
    BasicServiceService.prototype.createUser = function (user) {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.post(this.url + "/user", user, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_1.of(err.json());
        }));
    };
    BasicServiceService.prototype.loginUser = function (userLoginInfo) {
        console.log("this is login user info", userLoginInfo);
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.post(this.url + "/user/login", userLoginInfo, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_1.of(err.json());
        }));
    };
    BasicServiceService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], BasicServiceService);
    return BasicServiceService;
}());
exports.BasicServiceService = BasicServiceService;


/***/ }),

/***/ "./src/app/service/productinfo.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/productinfo.service.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var operators_1 = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var rxjs_2 = __webpack_require__(/*! ../../../node_modules/rxjs */ "./node_modules/rxjs/_esm5/index.js");
var environment_1 = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var ProductinfoService = /** @class */ (function () {
    function ProductinfoService(http) {
        this.http = http;
        this.url = environment_1.environment.serverUrl;
        this.searchValue = new rxjs_2.BehaviorSubject([]);
        // this.token =localStorage.getItem('token');
        // console.log("token",this.token);
    }
    ProductinfoService.prototype.createAuthorizationHeader = function (headers) {
        this.token = localStorage.getItem('token');
        console.log("token", this.token);
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        headers.append("token", this.token);
        return new http_1.RequestOptions({ headers: headers });
    };
    ProductinfoService.prototype.createAuthorizationWithoutToken = function (headers) {
        // this.token =localStorage.getItem('token');
        // console.log("token",this.token);
        headers.append("Content-Type", "application/json");
        headers.append("Accept", "application/json");
        // headers.append("token",this.token)
        return new http_1.RequestOptions({ headers: headers });
    };
    ProductinfoService.prototype.globalSearch = function (value) {
        // let val =Object.values(value);
        console.log(value);
        this.searchValue.next(value);
        // this.searchValue =value;
    };
    ProductinfoService.prototype.getProduct = function () {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationWithoutToken(headers);
        return this.http.get(this.url + '/get_product_list', options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_1.of(err.json());
        }));
    };
    ProductinfoService.prototype.getSearchProduct = function (search_value) {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.get(this.url + '/global_search/' + search_value, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_1.of(err.json());
        }));
    };
    ProductinfoService.prototype.getParticularProduct = function (pid) {
        var headers = new http_1.Headers();
        var options = this.createAuthorizationHeader(headers);
        return this.http.get(this.url + '/get_particular_product/' + pid, options).pipe(operators_1.map(function (res) { return res.json(); }), operators_1.catchError(function (err) {
            return rxjs_1.of(err.json());
        }));
    };
    ProductinfoService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], ProductinfoService);
    return ProductinfoService;
}());
exports.ProductinfoService = ProductinfoService;


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false,
    baseurl: "http://192.168.1.43:4000/",
    serverUrl: 'http://192.168.1.43:4000/user'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
var environment_1 = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/rudra/Music/node project according to doc/forntEnd/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map