const express = require('express');
const path = require('path');
const http = require('http');

const app = express();
var port = process.env.port || 4200;
app.use(express.static(path.join(__dirname, 'forntEnd/dist/dempfornt')));
const server = http.createServer(app);

server.listen(port,() => console.log('server is running...'))
