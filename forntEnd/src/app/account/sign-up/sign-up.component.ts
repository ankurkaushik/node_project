import { Component, OnInit } from '@angular/core';
import { BasicServiceService } from '../../service/basic-service.service';
import { User} from '../../model/user';
import { FormBuilder, Validators,FormControl  } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public user : User = {
     first_name : '',
     last_name :'',
     email : '',
     password : '',
     username : '',
  };
  // userForm: any;
  errMsg : string ;
  constructor(private basicService: BasicServiceService,
               private route :Router
    // private formBuilder: FormBuilder
    ) {
    //  this.userForm = this.formBuilder.group({
    //     first_name : ['', Validators.required],
    //     last_name : ['', Validators.required],
    //     email : ['', Validators.required],
    //     username : ['', Validators.required],
    //     password : ['', Validators.required]
        
    //  })
   }

  ngOnInit() {

    
  }

 

  signUpUser(){
  
       this.basicService.createUser(this.user).subscribe((data) => {
         
           if(data.err){
              this.errMsg = data.err;
           }else{
             let userloginInfo ={
                email : this.user.email,
                password : this.user.password
             }
              this.basicService.loginUser(userloginInfo).subscribe((data) =>{
                  console.log('this is data',data);
                  if(data.token){
                       localStorage.setItem('token',data.token);
                       localStorage.setItem('username',this.user.username);
                        this.route.navigateByUrl('');
                  }
              })
           }
       })
  }

}
