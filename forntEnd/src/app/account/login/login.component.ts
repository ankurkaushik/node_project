import { Component, OnInit } from '@angular/core';
import { BasicServiceService } from '../../service/basic-service.service';
import { LoginUser} from '../../model/user';
import { RouterModule, Routes, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login_user : LoginUser = {
     email : '',
     password : ''  
  }
  constructor(private basicService: BasicServiceService, private route :Router) { }

  ngOnInit() {
    
  }

  loginUser(){
     console.log("this is login user",this.login_user);
     this.basicService.loginUser(this.login_user).subscribe((data) =>{
          if(data.token){
             console.log(data);
             localStorage.setItem('token',data.token)
             localStorage.setItem('username',data.userInfo.userName);
             localStorage.setItem('user_id',data.userInfo._id)
             this.basicService.dataAfterLogin(data.userInfo);
             this.route.navigateByUrl('');
          }
     })

  }

}
