import { Component, OnInit } from '@angular/core';
import { ProductinfoService} from '../../service/productinfo.service';

@Component({
  selector: 'app-serachproduct',
  templateUrl: './serachproduct.component.html',
  styleUrls: ['./serachproduct.component.css']
})
export class SerachproductComponent implements OnInit {
  productArry : any = [];
  showSearch : boolean = true;
  url :string = "http://192.168.1.43:4000/";
  constructor(private prodService :ProductinfoService) {}

  ngOnInit() {
      this.prodService.searchValue.subscribe(val =>{
          console.log("value on",val)
          if(val.length == 0){
             this.showSearch = true;
          }else{
          this.prodService.getSearchProduct(val).subscribe((data) =>{
               console.log(data);
               if(data.length == 0){
                 this.showSearch = true
               }else{
                 this.showSearch = false;
               this.productArry = data;
               }
          })
        }
      })

  }

}
