import { Component, OnInit,ViewChild } from '@angular/core';
import { ProductinfoService} from '../../service/productinfo.service';
import { RouterModule,ActivatedRoute, Routes, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from "ngx-stripe";
 

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.css']
})
export class BuyProductComponent implements OnInit {
  buyId : number;
  buyProductList : any =[];
  url= environment.baseurl;
  totalPrice : number =0;
  constructor(private route : ActivatedRoute,private prodService: ProductinfoService) {
    route.params.subscribe(params=>{
      
      this.buyId = params['id'];
      console.log("this is buy  id",this.buyId);

  })

   }

  ngOnInit() {
     this.prodService.getBuyProductList(this.buyId).subscribe((data)=>{
        console.log(data);
        for (let i = 0; i< data.length; i++) {
          this.totalPrice = this.totalPrice+ data[i].productPrice; 
          
        }
        this.buyProductList =data;
          console.log("this is buy product",this.totalPrice);
     })
  }
  
  openCheckout() {
    var handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_0MF75Pq7dhQwXem4WRKSxpRE00IePHzNBm',
      locale: 'auto',
      token: function (token: any) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    handler.open({
      name: 'Demo Site',
      description: '2 widgets',
      amount: 2000
    });
  }

}
