import { Component, OnInit } from '@angular/core';
import { ProductinfoService} from '../../service/productinfo.service';
import { RouterModule,ActivatedRoute, Routes, Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  pid: any;
  url= environment.baseurl;
  productInfo :any;

  constructor(private route : ActivatedRoute,private prodService: ProductinfoService,private router:Router) { 
    route.params.subscribe(params=>{
      
      this.pid = params['id'];

  })
}

  ngOnInit() {
    this.prodService.getParticularProduct(this.pid).subscribe((data)=>{
         this.productInfo = data;
    })

  }

  addBuyProduct(){
      var uid = localStorage.getItem('user_id');
      this.prodService.buyNowProduct(uid,this.pid).subscribe((data)=>{
         this.router.navigateByUrl('buy_product/'+data[0]._id);

     })
  }


}
