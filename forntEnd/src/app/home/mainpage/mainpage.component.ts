import { Component, OnInit } from '@angular/core';
import { ProductinfoService} from '../../service/productinfo.service';
import { AddCartService } from  '../../service/add-cart.service';
import { environment } from '../../../environments/environment';



@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  imagesUrl: string[];
  productArry : any = [];
  p: number = 1;
  url = environment.baseurl;
  constructor(
    private prodService :ProductinfoService,
    private addCartSer : AddCartService
  ) { }

  ngOnInit() {

    this.prodService.getProduct().subscribe((data) => {
        this.productArry = data;
    })
    
  
  }

  addToCart(value){
      this.addCartSer.addProduct(value);
  }

}
