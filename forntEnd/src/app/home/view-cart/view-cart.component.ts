import { Component, OnInit } from '@angular/core';
import { AddCartService } from  '../../service/add-cart.service';
import { environment } from "../../../environments/environment";


@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.css']
})
export class ViewCartComponent implements OnInit {
  AllCartItem : any =[];
  productTotal : number = 1;
  userId : string;
  url= environment.baseurl;
  constructor(
    private addCartSer : AddCartService
  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem('user_id');
    this.addCartSer.getCartItem(this.userId).subscribe((data)=>{
        this.AllCartItem =data.addToCart;
    })
    
  }

  inc(){
    this.productTotal = this.productTotal +1;
  }
  dec(){
    this.productTotal = this.productTotal -1;
  }

  removeToCart(product){
       this.addCartSer.removeItemFormCart(this.userId,product).subscribe((data)=>{
         if(data == true){
          this.userId = localStorage.getItem('user_id');
          this.addCartSer.getCartItem(this.userId).subscribe((data)=>{
              this.AllCartItem =data.addToCart;
          })
         }else{

         }
       })
  }
}
