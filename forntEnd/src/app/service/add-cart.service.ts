import { Injectable } from '@angular/core';
import { BehaviorSubject } from "../../../node_modules/rxjs";
import { Http,RequestOptions, Headers, Response } from '@angular/http'
import { map, catchError } from "rxjs/operators";
import { Observable,of } from "rxjs";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddCartService {
  addCart  = new BehaviorSubject<any[]>([]);
  url = environment.serverUrl+'/user/';
  token : string;
  constructor(
    private http: Http
  ) {
      // this.token =localStorage.getItem('token');
      // console.log("token",this.token);
   }

   createAuthorizationHeader(headers: Headers) {
    this.token =localStorage.getItem('token');
    console.log("token",this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("token",this.token)
    return new RequestOptions({ headers: headers });
  }

  addProduct(value: any){
      // this.addCart.next(this.addCart.getValue().concat([value])); //this is for making array
      this.addCart.next(value);
  }

  saveAddCartData(id: any, obj:any):Observable<any> {
    console.log("this is object",obj);
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.post(this.url + "/add_to_cart/"+id,obj, options).pipe(
      map((res: any) => res.json()),
      catchError(err => {
        return of(err.json());
      })
    );
  }


  getCartItem(id:any){
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.get(this.url+'/get_cart_item/'+id,options).pipe(
      map((res: Response) => res.json()),
    catchError(err => {
      return of(err.json());
    })
    )
  }

  removeItemFormCart(Id:any,obj:any){
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.post(this.url + "/remove_to_cart/"+Id,obj, options).pipe(
      map((res: any) => res.json()),
      catchError(err => {
        return of(err.json());
      })
    )};

}
