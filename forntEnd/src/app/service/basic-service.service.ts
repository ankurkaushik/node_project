import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http,RequestOptions, Headers ,Response} from '@angular/http'
import { map, catchError } from "rxjs/operators";
import { Observable,of } from "rxjs";
import { User, LoginUser } from '../model/user';
import { BehaviorSubject } from "../../../node_modules/rxjs";



@Injectable({
  providedIn: 'root'
})
export class BasicServiceService {
  url = environment.serverUrl
  userDataSource = new BehaviorSubject<any[]>([]);

  constructor(private http: Http) {
   
   }

   createAuthorizationHeader(headers: Headers) {
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    return new RequestOptions({ headers: headers });
  }

 dataAfterLogin(data){
  this.userDataSource.next(data);
 }

  createUser(user : User):Observable<any> {
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.post(this.url + "/user",user, options).pipe(
      map((res: any) => res.json()),
      catchError(err => {
        return of(err.json());
      })
    );
  }

 loginUser(userLoginInfo: LoginUser):Observable<any> {
   console.log("this is login user info",userLoginInfo);
  let headers = new Headers();
  let options = this.createAuthorizationHeader(headers);
  return this.http.post(this.url + "/user/login",userLoginInfo, options).pipe(
    map((res: any) => res.json()),
    catchError(err => {
      return of(err.json());
    })
  );
 }

}
