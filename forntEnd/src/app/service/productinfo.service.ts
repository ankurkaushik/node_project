import { Injectable } from '@angular/core';
import { Http,RequestOptions, Headers, Response } from '@angular/http'
import { map, catchError } from "rxjs/operators";
import { Observable,of } from "rxjs";
import { BehaviorSubject } from "../../../node_modules/rxjs";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductinfoService {
  url = environment.serverUrl+"/user/";
  token : string;
  searchValue  = new BehaviorSubject<any[]>([]);
  constructor(
    private http: Http
  ) {
      // this.token =localStorage.getItem('token');
      // console.log("token",this.token);
   }

  createAuthorizationHeader(headers: Headers) {
    this.token =localStorage.getItem('token');
    console.log("token",this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("token",this.token)
    return new RequestOptions({ headers: headers });
  }

  createAuthorizationWithoutToken(headers: Headers) {
    // this.token =localStorage.getItem('token');
    // console.log("token",this.token);
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    // headers.append("token",this.token)
    return new RequestOptions({ headers: headers });
  }

  globalSearch(value){
      // let val =Object.values(value);
      console.log(value)
      this.searchValue.next(value);
      // this.searchValue =value;

  }

  getProduct(){
      let headers = new Headers();
      let options = this.createAuthorizationWithoutToken(headers);
      return this.http.get(this.url+'/get_product_list',options).pipe(
        map((res: Response) => res.json()),
      catchError(err => {
        return of(err.json());
      })
      )
  }

  getSearchProduct(search_value:any){
    let headers = new Headers();
      let options = this.createAuthorizationHeader(headers);
      return this.http.get(this.url+'/global_search/'+search_value,options).pipe(
        map((res: Response) => res.json()),
      catchError(err => {
        return of(err.json());
      })
      )
  }


  getParticularProduct(pid:any){
    let headers = new Headers();
      let options = this.createAuthorizationHeader(headers);
      return this.http.get(this.url+'/get_particular_product/'+pid,options).pipe(
        map((res: Response) => res.json()),
      catchError(err => {
        return of(err.json());
      })
      )
  }

  buyNowProduct(uid:any,pid:any){
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.get(this.url+'/buy_now/'+uid+'/'+pid,options).pipe(
      map((res: Response) => res.json()),
    catchError(err => {
      return of(err.json());
    })
    )
  }


  getBuyProductList(buyId:any){
    let headers = new Headers();
    let options = this.createAuthorizationHeader(headers);
    return this.http.get(this.url+'list_of_buyItem/'+buyId,options).pipe(
      map((res: Response) => res.json()),
    catchError(err => {
      return of(err.json());
    })
    )
  }

 getUserDeities(uid:any){
  let headers = new Headers();
  let options = this.createAuthorizationHeader(headers);
  return this.http.get(this.url+'get_user_info/'+uid,options).pipe(
    map((res: Response) => res.json()),
  catchError(err => {
    return of(err.json());
  })
  )

 }


}
