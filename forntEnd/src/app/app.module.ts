import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';

import { MainpageComponent } from '../app/home/mainpage/mainpage.component';
import { SignUpComponent } from '../app/account/sign-up/sign-up.component';
import { LoginComponent } from '../app/account/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule }from '@angular/common';
// import { Http } from '@angular/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {SlideshowModule} from 'ng-simple-slideshow';
import { SidenavComponent } from './layout/sidenav/sidenav.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { SerachproductComponent } from './home/serachproduct/serachproduct.component';
import { ViewCartComponent } from './home/view-cart/view-cart.component';
import { ProductDetailsComponent } from './home/product-details/product-details.component';
import { BuyProductComponent } from './home/buy-product/buy-product.component'; 
// import { Module as StripeModule } from "stripe-angular"
import { NgxStripeModule } from 'ngx-stripe';

const routes: Routes = [
  {
      path: '',
      component: MainpageComponent,
  },
  {
     path: 'login',
     component : LoginComponent
  },
  {
    path: 'signup',
    component : SignUpComponent
  },
  {
    path:'search_product',
    component : SerachproductComponent
  },
  {
    path: 'view_cart',
    component: ViewCartComponent
  },
  {
    path: 'product_details/:id',
    component:ProductDetailsComponent
  },
  {
    path: 'buy_product/:id',
    component:BuyProductComponent
  }
];



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainpageComponent,
    SignUpComponent,
    LoginComponent,
    SidenavComponent,
    SerachproductComponent,
    ViewCartComponent,
    ProductDetailsComponent,
    BuyProductComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    // Http
    HttpModule,
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    SlideshowModule,
    NgxPaginationModule
    
  ],
  exports:[
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
