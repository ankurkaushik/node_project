import { Component, OnInit } from '@angular/core';
import { BasicServiceService } from '../../service/basic-service.service';
import { ProductinfoService} from '../../service/productinfo.service';
import { RouterModule, Routes, Router } from '@angular/router';
import { AddCartService } from  '../../service/add-cart.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showMenu : boolean ;
  userInfo : any;
  searchInput : string;
  addToCartCount : number =0;
  countCartAPi : number ;
  newArryValue : any =[];
  pushCart :any = []
  constructor(
    private basicService: BasicServiceService,
    private prodService :ProductinfoService,
    private route :Router,
    private addCartSer : AddCartService
  ) { 
 
  }

  ngOnInit() {
      this.basicService.userDataSource.subscribe(val =>{
          
          if(val.length !== 0){
            console.log("this is value",Object.values(val));
            let newUserInfo = Object.values(val);
            this.showMenu =true;
            this.userInfo = newUserInfo[6];

            let userId = localStorage.getItem('user_id');
            console.log(userId)
            this.addCartSer.getCartItem(userId).subscribe((data)=>{
                console.log("strting cart value",data.addToCart);
                this.countCartAPi = data.addToCart.length;
                // this.newArryValue.push(data.addToCart);
                // console.log(this.countCartAPi);
                this.addToCartCount =this.countCartAPi
            })
          }else{
            console.log("no")
            let userinfo = localStorage.getItem('username');
            console.log(userinfo);
            this.userInfo =userinfo;
            if(userinfo){
               this.showMenu =true
            }else{
            this.showMenu =false;
            }
          }
      })
      console.log("this is testing")
      let userId = localStorage.getItem('user_id');
      this.addCartSer.getCartItem(userId).subscribe((data)=>{
          console.log("strting cart value",data.addToCart);
          this.countCartAPi = data.addToCart.length;
          // this.newArryValue.push(data.addToCart);
          // console.log(this.countCartAPi);
          this.addToCartCount =this.countCartAPi
      })
      this.addCartSer.addCart.subscribe(val =>{
           console.log("this is value",val);
           
          //  this.pushCart.push(val);
          //  console.log(this.pushCart.length);
          //  this.addToCartCount = this.pushCart.length;
           if(val.length == 1){
             console.log('no one')
          
            this.addToCartCount = 0
            this.pushCart.length =0
            //  console.log("no value",this.countCartAPi);
              //  this.addToCartCount =this.countCartAPi
           }else{
            this.pushCart.push(val);
            console.log(this.pushCart.length);
            // this.addToCartCount = this.pushCart.length;
              this.addToCartCount = this.countCartAPi+ this.pushCart.length-1;
              // this.newArryValue[0].push(val);
              // console.log("add to card api",this.newArryValue);
              let userId = localStorage.getItem('user_id');
              this.addCartSer.saveAddCartData(userId,val).subscribe((data)=>{
               
                 })
           }
      })
      // let userId = localStorage.getItem('user_id');
      // this.addCartSer.getCartItem(userId).subscribe((data)=>{
         
      // })
  }

  Logout(){
      localStorage.removeItem('username');
      localStorage.removeItem('token');
      localStorage.removeItem('user_id');
      this.showMenu =false;
  }

  search(){
      console.log("search",this.searchInput);
      this.prodService.globalSearch(this.searchInput);
      this.route.navigateByUrl('/search_product')
  }

  showBuyList(){
     console.log("this is user buying  id",this.userInfo)
     var uid = localStorage.getItem('user_id');
     console.log("-----",uid);
     this.prodService.getUserDeities(uid).subscribe((data) =>{
          console.log("this is data",data);
          
     })

  }

}
