var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var addToCartSchema = new Schema({
      uid : mongoose.Schema.Types.ObjectId,
      addToCart : {
           type : Array,
           default :[]
      }
});

var addToCartSchema = mongoose.model('addToCartSchema',addToCartSchema);

module.exports = {
    addToCartSchema : addToCartSchema
}