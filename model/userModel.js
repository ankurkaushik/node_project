var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
      firstName :String,
      lastName : String,
      userName : {type:String , unique:true},
      email : String,
      password : String,
      userBuyInformation :{
            type:Array,
            default:[]
      }
});

var userData = mongoose.model('userData',userSchema);

module.exports = {
      userData : userData
}