var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    productName :{
        type:String,
        unique:true,
        required: [true,'product name  required'],
    },
    BuyInfo:[{
      type: mongoose.Schema.Types.ObjectId,
      ref : 'addToBuySchema'
    }],
    BuyInfoIds:{
       type:Array,
       default:[]
    },
    productQty : Number,
    productBrand : String,
    productPrice : {
        type:Number,
        required: [true,'product price  required'],
        min : [200,'your price is less then 200, pleas more then 200'],
        max: [20000, 'your price is more then 20000']
    },
    ImageUrl : {
        type: String,
        required: [true,'product Image required'],
    },
    KeyPoint :{
        keyPoint1: {
          type: String,
          required: [true,'product key point1  required'],
        },
        keyPoint2: {
            type: String,
            required: [true,'product key point2  required'],
        },
        keyPoint3 :{
            type: String,
            required: [true,'product key point3  required'],
        }
    },
    created_on : {type:Date,default:Date.now},
   
});

var productData = mongoose.model('productData',productSchema);

module.exports = {
    productData : productData
}