var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var addToBuySchema = new Schema({
      uid : mongoose.Schema.Types.ObjectId,
      addToBuy : [{
           type : mongoose.Schema.Types.ObjectId,
           ref: 'productData'
      }]
});

var addToBuySchema = mongoose.model('addToBuySchema',addToBuySchema);

module.exports = {
    addToBuySchema : addToBuySchema
}