var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var cookiesParser = require('cookie-parser');
var mongoose = require('mongoose');
var userRoute = require('./routing/userRouting');
var path = require('path');
var cors = require('cors')
// var host = process.env.host
var port = process.env.port || 4000;

mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true},function(err,result){
     if(err){
         console.log("err is connection with mongodb and error :",err);
     } 
     console.log("mongoDB are successfully connected !");
});

// app.listen(port, function(){
//     console.log("server are run at "+ port);
// })

// app.use('/user',userRoute);
app.use(cors());
app.use(bodyParser.json())
app.use(cookiesParser())
app.use(express.static(path.join(__dirname, 'forntEnd/dist/dempfornt')));
app.use(express.static(path.join(__dirname, 'public/')));

// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'forntEnd/dist/dempfornt/index.html'));
// });


app.use('/user',userRoute);


app.listen(port, function(){
      console.log("server are run at "+ port);
})